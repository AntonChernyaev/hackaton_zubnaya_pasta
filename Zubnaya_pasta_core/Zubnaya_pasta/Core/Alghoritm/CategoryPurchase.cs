﻿using System;

namespace Core.Alghoritm
{
    public class CategoryPurchase
    {
        public CategoryPurchase(string categoryName, long averagePurchaseTimeSeconds,  DateTime firstPurchaseTime, DateTime lastPurchaseTime, int purchaseCount)
        {
            CategoryName = categoryName;
            AveragePurchaseTimeSeconds = averagePurchaseTimeSeconds;
            LastPurchaseTime = lastPurchaseTime;
            FirstPurchaseTime = firstPurchaseTime;
            PurchaseCount = purchaseCount;
        }

        public string CategoryName { get; set; }
        public long AveragePurchaseTimeSeconds { get; set; }
        public DateTime FirstPurchaseTime { get; set; }
        public DateTime LastPurchaseTime { get; set; }
        public int PurchaseCount { get; set; }
    }
}