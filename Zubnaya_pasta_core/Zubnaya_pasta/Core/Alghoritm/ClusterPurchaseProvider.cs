﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using System.Data.Entity;


namespace Core.Alghoritm
{
	public class ClusterPurchaseProvider : IForgottenPurchasesProvider
	{
		public async Task<List<CategoryPurchase>> GetForgottenPurchasesAsync(int userId, float daysSinceLastPurchase, List<string> currentPurchasesNames)
		{
			using (var dbContext = new ProductContext())
			{
				var userOrders = await dbContext.Orders
					.Include(o => o.OrderItems)
					.Where(o => o.UserId == userId)
					.OrderBy(o => o.DateTime)
					.ToListAsync();

				var catToFrequncy = GetCatToFreq(userOrders);
				//catToFrequncy.ForEach(x => Console.WriteLine($"{x.Item1}: {x.Item2:F2}"));

				var catToCount = GetCatToCount(userOrders);
				//catToCount.ForEach(x => Console.WriteLine($"{x.Item1}: {x.Item2}"));

				var catToTimeSinceLast = GetCatToTimeSinceLast(userOrders, daysSinceLastPurchase);
				//catToTimeSinceLast.ForEach(x => Console.WriteLine($"{x.Item1}: {x.Item2:F2}"));

				var valuedCats = GetValuedCats(catToCount, catToFrequncy, catToTimeSinceLast);
				//valuedCats.ForEach(x => Console.WriteLine($"{x.Item1}: {x.Item2:F2}"));

				var inCartCats = await dbContext.Categories
					.Where(x => currentPurchasesNames.Contains(x.Name))
					.Select(x => x.CategoryId)
					.ToListAsync();

				var selectedCats = valuedCats.Where(x => !inCartCats.Contains(x.Item1) && x.Item2 > 0.5f).ToList();

				return new List<CategoryPurchase>();
			}

		}

		private List<Tuple<int, double>> GetValuedCats(List<Tuple<int, int>> catToCount, List<Tuple<int, double>> catToFrequncy, List<Tuple<int, double>> catToTimeSinceLast)
		{
			var result = new List<Tuple<int, double>>();
			var cats = catToCount.Select(x => x.Item1).ToList();

			result = cats
				.Select(c =>
				{
					var countVal = catToCount.Where(x => x.Item1 == c).FirstOrDefault()?.Item2 ?? 0;
					var freqVal = catToFrequncy.Where(x => x.Item1 == c).FirstOrDefault()?.Item2 ?? double.MaxValue;
					var timeVal = catToTimeSinceLast.Where(x => x.Item1 == c).FirstOrDefault()?.Item2 ?? double.MaxValue;

					double megaValue = (countVal > 5 ? 1 : 0) * (timeVal / freqVal);

					return new Tuple<int, double>(c, megaValue);
				})
				.OrderByDescending(x => x.Item2)
				.ToList();
			return result;
		}

		private List<Tuple<int, double>> GetCatToFreq(List<Order> userOrders)
		{
			var result = new List<Tuple<int, double>>();

			var userCats = userOrders
				.Select(x => x.OrderItems.AsEnumerable())
				.Aggregate((x, y) => x.Union(y))
				.Select(x => x.CategoryId)
				.Distinct().ToList();

			foreach (var c in userCats)
			{
				var ordersWithCat = userOrders.Where(x => x.OrderItems.Any(y => y.CategoryId == c)).OrderBy(x => x.DateTime).ToList();
				var diffs = new List<double>();
				for (int i = 0; i < ordersWithCat.Count - 1; ++i)
				{
					diffs.Add((ordersWithCat[i + 1].DateTime - ordersWithCat[i].DateTime).TotalDays);
				}

				if (!diffs.Any()) continue;
				var freq = diffs.Average();
				result.Add(new Tuple<int, double>(c, freq));
			}

			result = result.OrderBy(x => x.Item2).ToList();

			return result;
		}

		private List<Tuple<int, int>> GetCatToCount(List<Order> userOrders)
		{
			var result = new List<Tuple<int, int>>();

			var userCats = userOrders
				.Select(x => x.OrderItems.AsEnumerable())
				.Aggregate((x, y) => x.Union(y))
				.Select(x => x.CategoryId)
				.Distinct().ToList();

			foreach (var c in userCats)
			{
				var ordersWithCat = userOrders.Where(x => x.OrderItems.Any(y => y.CategoryId == c)).OrderBy(x => x.DateTime).ToList();

				result.Add(new Tuple<int, int>(c, ordersWithCat.Count));
			}

			result = result.OrderByDescending(x => x.Item2).ToList();

			return result;
		}

		private List<Tuple<int, double>> GetCatToTimeSinceLast(List<Order> userOrders, float daysSinceLast)
		{
			var result = new List<Tuple<int, double>>();

			var lastOrderTime = userOrders.Max(x => x.DateTime);
			var currentOrderTime = lastOrderTime.AddDays(daysSinceLast);

			var userCats = userOrders
				.Select(x => x.OrderItems.AsEnumerable())
				.Aggregate((x, y) => x.Union(y))
				.Select(x => x.CategoryId)
				.Distinct().ToList();

			foreach (var c in userCats)
			{
				var ordersWithCat = userOrders.Where(x => x.OrderItems.Any(y => y.CategoryId == c)).OrderBy(x => x.DateTime).ToList();
				var lastCatTime = ordersWithCat.Max(x => x.DateTime);
				var diff = (currentOrderTime - lastCatTime).TotalDays;
				result.Add(new Tuple<int, double>(c, diff));
			}

			result = result.OrderByDescending(x => x.Item2).ToList();

			return result;
		}
	}
}
