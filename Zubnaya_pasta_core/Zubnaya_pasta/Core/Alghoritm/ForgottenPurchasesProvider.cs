﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using System.Data.Entity;

namespace Core.Alghoritm
{
	public class ForgottenPurchasesProvider : IForgottenPurchasesProvider
	{
		private const int MinAverageDaysForForgotten = 10;
		private const int MinPurchasesForForgotten = 5;
		private const double MaxAverageFault = 1.5;
		public async Task<List<CategoryPurchase>> GetForgottenPurchasesAsync(int userId, float daysSinceLastPurchase, List<string> currentPurchasesNames)
		{
			using (var dbContext = new ProductContext())
			{
				var user = await dbContext.Users.Include(o => o.Orders).FirstOrDefaultAsync(u => u.UserId == userId);
				var userOrders = await dbContext.Orders.Include(o => o.OrderItems).Where(o => o.UserId == user.UserId).OrderBy(o => o.DateTime)
					.ToListAsync();
				var userCategoryPurchases = await GetUserCategoryPurchases(dbContext, userOrders);
				var currentPurchasesUpperInvariant =
					currentPurchasesNames.Select(purchaseName => purchaseName.ToUpperInvariant()).ToList();
				var currentTime = userOrders.Last().DateTime.AddDays(daysSinceLastPurchase);
				var forgottenPurchases =
					GetForgottenPurchasesInternal(currentPurchasesUpperInvariant, userCategoryPurchases, currentTime);
				return forgottenPurchases;
			}
		}

		private async Task<List<CategoryPurchase>> GetUserCategoryPurchases(ProductContext dbContext, List<Order> orders)
		{
			var averageCategoryPurchases = new Dictionary<string, CategoryPurchase>();
			foreach (var order in orders)
			{
				foreach (var orderItem in order.OrderItems)
				{
					var orderItemCopy = await dbContext.OrderItems.Include(o => o.Category)
						.FirstOrDefaultAsync(o => o.OrderItemId == orderItem.OrderItemId);
					CategoryPurchase categoryPurchase;
					var isAlreadyAddedCategoryPurchase =
						averageCategoryPurchases.TryGetValue(orderItemCopy.Category.Name, out categoryPurchase);
					if (!isAlreadyAddedCategoryPurchase)
					{
						categoryPurchase = new CategoryPurchase(orderItemCopy.Category.Name, 0,
							order.DateTime, order.DateTime, 1);
						averageCategoryPurchases[orderItemCopy.Category.Name] = categoryPurchase;
						continue;
					}

					categoryPurchase.LastPurchaseTime = order.DateTime;
					if (categoryPurchase.LastPurchaseTime == categoryPurchase.FirstPurchaseTime)
						continue;

					categoryPurchase.PurchaseCount++;
					categoryPurchase.AveragePurchaseTimeSeconds =
						(long)(categoryPurchase.LastPurchaseTime - categoryPurchase.FirstPurchaseTime).TotalSeconds /
						categoryPurchase.PurchaseCount;
				}
			}

			return averageCategoryPurchases.Values.Where(category => category.PurchaseCount >= MinPurchasesForForgotten).ToList();
		}

		private List<CategoryPurchase> GetForgottenPurchasesInternal(List<string> currentPurchases,
			List<CategoryPurchase> userCategoryPurchases, DateTime currentTime)
		{
			var forgottenPurchases = new List<CategoryPurchase>();
			foreach (var userCategoryPurchase in userCategoryPurchases)
			{
				if (currentPurchases.Contains(userCategoryPurchase.CategoryName))
					continue;

				var timeFromLastPurchase = currentTime - userCategoryPurchase.LastPurchaseTime;

				if (TimeSpan.FromSeconds(userCategoryPurchase.AveragePurchaseTimeSeconds).Days < MinAverageDaysForForgotten)
					continue;

				var averagePurchaseTimeSeconds = (userCategoryPurchase.AveragePurchaseTimeSeconds * MaxAverageFault);
				if (timeFromLastPurchase.TotalSeconds < averagePurchaseTimeSeconds)
					continue;

				forgottenPurchases.Add(userCategoryPurchase);
			}
			return forgottenPurchases;
		}
	}
}