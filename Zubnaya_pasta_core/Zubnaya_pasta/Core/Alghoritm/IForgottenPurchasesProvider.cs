﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Alghoritm
{
	public interface IForgottenPurchasesProvider
	{
		Task<List<CategoryPurchase>> GetForgottenPurchasesAsync(
			int userId,
			float daysSinceLastPurchase,
			List<string> currentPurchasesNames);
	}
}