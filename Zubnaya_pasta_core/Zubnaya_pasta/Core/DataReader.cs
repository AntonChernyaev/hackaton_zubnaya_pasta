using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Models;
using Test.RawData;

namespace Core
{
    public static class DataReader
    {
        public static void FillDb()
        {
            var orders = GetRawOrders();
            var products = GetRawProducts();
            var aisles = GetRawAisles();

            var groupBy = orders.GroupBy(o => o.UserId).Select(g => new
            {
                UserId = g.Key,
                Count = g.Count()
            }).OrderByDescending(g => g.Count).ToList();

            var usersSort = groupBy.Take(1).Select(g => g.UserId).ToList();
            var ordersSort = orders.Where(o => usersSort.Contains(o.UserId)).ToList();
            var ordersIdSort = ordersSort.Select(o => o.OrderId).ToList();
            var dictionary = ordersIdSort.ToDictionary(o => o);

            var orderToProducts = GetRawOrderToProducts(dictionary);

            var aislesMap = aisles.ToDictionary(a => a.AisleId);
            var productsMap = products.ToDictionary(p => p.ProductId);

            using (var pc = new ProductContext())
            {
                foreach (var a in aisles)
                {
                    var category = new Category()
                    {
                        CategoryId = a.AisleId,
                        Name = a.Name
                    };
                    pc.Categories.Add(category);
                }

                foreach (var userId in usersSort)
                {
                    var user = new User()
                    {
                        UserId = userId
                    };
                    pc.Users.Add(user);
                }

                var previousDate = DateTime.UtcNow.AddYears(-2);
                ordersSort.Sort((x, y) => x.OrderNumber.CompareTo(y.OrderNumber));
                foreach (var o in ordersSort)
                {
                    var d = o.OrderNumber == 1 ? DateTime.UtcNow.AddYears(-2) : previousDate.AddDays(o.DaysSinceLastOrder);
                    previousDate = d;
                    var order = new Order()
                    {
                        OrderId = o.OrderId,
                        UserId = o.UserId,
                        DateTime = d,
						DaysSinceLast = o.DaysSinceLastOrder
                    };
                    pc.Orders.Add(order);
                }

                foreach (var orderToProduct in orderToProducts)
                {
                    var product = productsMap[orderToProduct.ProductId];
                    var aisle = aislesMap[product.AisleId];
                    var orderItems = new OrderItem()
                    {
                        OrderId = orderToProduct.OrderId,
                        CategoryId = aisle.AisleId
                    };
                    pc.OrderItems.Add(orderItems);
                }

                pc.SaveChanges();
            }
        }

        private static List<RawOrderToProduct> GetRawOrderToProducts(Dictionary<int, int> dictionary)
        {
            var orderToProductPath = @"..\..\..\dataset\instacart\order_products__prior.csv";

            var otp = new List<RawOrderToProduct>();
            var file = new FileInfo(orderToProductPath);

            using (var stream = file.OpenRead())
            using (var reader = new StreamReader(stream))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var rawOrderToProduct = new RawOrderToProduct(line);
                    int ov;
                    if (dictionary.TryGetValue(rawOrderToProduct.OrderId, out ov))
                        otp.Add(rawOrderToProduct);
                }
            }

            Console.WriteLine($"OrderToProduct loaded: {otp.Count}");
            return otp;
        }

        private static List<RawAisle> GetRawAisles()
        {
            var aislesPath = @"..\..\..\dataset\instacart\aisles.csv";

            var aisles = new List<RawAisle>();
            var file = new FileInfo(aislesPath);

            using (var stream = file.OpenRead())
            using (var reader = new StreamReader(stream))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    aisles.Add(new RawAisle(line));
                }
            }

            Console.WriteLine($"Aisles loaded: {aisles.Count}");
            return aisles;
        }

        private static List<RawProduct> GetRawProducts()
        {
            var productsPath = @"..\..\..\dataset\instacart\products.csv";

            var products = new List<RawProduct>();
            var file = new FileInfo(productsPath);

            using (var stream = file.OpenRead())
            using (var reader = new StreamReader(stream))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    products.Add(new RawProduct(line));
                }
            }

            Console.WriteLine($"Products loaded: {products.Count}");
            return products;
        }

        private static List<RawOrder> GetRawOrders()
        {
            var ordersPath = @"..\..\..\dataset\instacart\orders.csv";

            var orders = new List<RawOrder>();
            var file = new FileInfo(ordersPath);

            using (var stream = file.OpenRead())
            using (var reader = new StreamReader(stream))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line.Contains("train")) continue;
                    orders.Add(new RawOrder(line));
                }
            }

            Console.WriteLine($"Orders loaded: {orders.Count}");
            return orders;
        }
    }
}