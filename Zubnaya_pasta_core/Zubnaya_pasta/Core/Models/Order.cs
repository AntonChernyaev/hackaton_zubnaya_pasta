using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models
{
	[Table("Order")]
	public class Order
	{
		[Key]
		public int OrderId { get; set; }

		public int UserId { get; set; }

		public User User { get; set; }

		public DateTime DateTime { get; set; }

		public float DaysSinceLast { get; set; }

		public ICollection<OrderItem> OrderItems { get; set; }
	}
}