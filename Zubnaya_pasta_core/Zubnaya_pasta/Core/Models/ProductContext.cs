using System.Data.Entity;
using SQLite.CodeFirst;

namespace Core.Models
{
    public class ProductContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<ProductContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer); 
        }
    }
}