using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        public int UserId { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}