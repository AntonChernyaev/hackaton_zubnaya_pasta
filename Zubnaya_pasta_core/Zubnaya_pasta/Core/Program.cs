﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Alghoritm;
using Core.Models;

namespace Core
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			//DataReader.FillDb();

			Console.WriteLine(" DataReader.FillDb() done");

			var provider = new ClusterPurchaseProvider();//new ForgottenPurchasesProvider();
			var forgottenPurchases = provider.GetForgottenPurchasesAsync(1, 7, new List<string>()).Result;
			forgottenPurchases.Sort((x, y) => x.PurchaseCount.CompareTo(y.PurchaseCount));
			var message = new StringBuilder();
			message.Append("Вы забыли купить: \n");
			foreach (var forgottenPurchase in forgottenPurchases)
				message.Append($"{forgottenPurchase.CategoryName} {TimeSpan.FromSeconds(forgottenPurchase.AveragePurchaseTimeSeconds)} {forgottenPurchase.PurchaseCount} \n");

			Console.WriteLine(message.ToString());
			Console.ReadKey();
		}

		private static void InitDb()
		{
			using (var db = new ProductContext())
			{
				//Users
				for (int i = 0; i < 3; i++)
				{
					var person = new User();
					db.Users.Add(person);
				}

				//Categories
				var categories = new List<string>
				{
					"Каша", "Подгузники", "Шампунь", "Творог", "Молоко", "Туалетная бумага", "Хлеб",
					"Макаранный изделия", "Зубная паста", "Напитки"
				};
				foreach (var categoryName in categories)
				{
					var category = new Category
					{
						Name = categoryName
					};
					db.Categories.Add(category);
				}

				//Orders

				db.SaveChanges();
			}
		}
	}
}