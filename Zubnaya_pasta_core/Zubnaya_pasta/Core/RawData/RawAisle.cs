﻿using System;

namespace Test.RawData
{
	public class RawAisle
	{
		public readonly int AisleId;
		public readonly string Name;

		public RawAisle(string line)
		{
			var tokens = line.Split(new[] { ',' }, 2, StringSplitOptions.RemoveEmptyEntries);
			AisleId = int.Parse(tokens[0]);
			Name = tokens[1];
		}
	}
}
