﻿using System;
using System.Globalization;

namespace Test.RawData
{
	public class RawOrder
	{
		public readonly int OrderId;
		public readonly int UserId;
		public readonly int DoW;
		public readonly int HoD;
		public int OrderNumber;
		public float DaysSinceLastOrder;

		public RawOrder(string line)
		{
			var tokens = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			OrderId = int.Parse(tokens[0]);
			UserId = int.Parse(tokens[1]);
			OrderNumber = int.Parse(tokens[3]);
			DoW = int.Parse(tokens[4]);
			HoD = int.Parse(tokens[5]);
			DaysSinceLastOrder = tokens.Length == 7 ? float.Parse(tokens[6], NumberStyles.Any, CultureInfo.InvariantCulture) : 0f;
		}
	}
}
