﻿using System;

namespace Test.RawData
{
	public class RawOrderToProduct
	{
		public readonly int OrderId;
		public readonly int ProductId;
		public readonly int OrderNum;
		public readonly bool Reordered;

		public RawOrderToProduct(string line)
		{
			var tokens = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			OrderId = int.Parse(tokens[0]);
			ProductId = int.Parse(tokens[1]);
			OrderNum = int.Parse(tokens[2]);
			Reordered = int.Parse(tokens[3]) == 1;
		}
	}
}
