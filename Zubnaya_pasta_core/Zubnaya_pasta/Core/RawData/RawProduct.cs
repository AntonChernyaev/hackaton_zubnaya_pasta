﻿using System;

namespace Test.RawData
{
	public class RawProduct
	{
		public readonly int ProductId;
		public readonly int AisleId;

		public RawProduct(string line)
		{
			var tokens = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			ProductId = int.Parse(tokens[0]);
			AisleId = int.Parse(tokens[tokens.Length-2]);
		}
	}
}
