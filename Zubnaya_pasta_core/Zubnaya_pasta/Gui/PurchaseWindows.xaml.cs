﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Core.Alghoritm;
using Core.Models;

namespace Gui
{
    public partial class PurchaseWindows : Window
    {
        private readonly ForgottenPurchasesProvider _forgottenPurchasesProvider;
        public PurchaseWindows()
        {
            InitializeComponent();
            using (var pc = new ProductContext())
            {
                var categories = pc.Categories.ToList();
                categoryCb.ItemsSource = categories;
            }
            
            _forgottenPurchasesProvider = new ForgottenPurchasesProvider();
        }

        private void AddPurchase_OnClick(object sender, RoutedEventArgs e)
        {
            if (categoryCb.SelectedIndex == 0)
                return;

            lbPurchases.Items.Add(categoryCb.SelectedItem);
        }

        private async void EndPurchases_OnClick(object sender, RoutedEventArgs e)
        {
            var currentPurchases = lbPurchases.Items.Cast<object>().Select(o => o.ToString()).ToList();
            await Task.Run(async () =>
            {
                var forgottenPurchases = await _forgottenPurchasesProvider.GetForgottenPurchasesAsync(1, currentPurchases);
                var message = new StringBuilder();
                message.Append("Вы забыли купить: \n");
                foreach (var forgottenPurchase in forgottenPurchases)
                    message.Append($"{forgottenPurchase.CategoryName} \n");

                MessageBox.Show(message.ToString());
            });
        }
    }
}